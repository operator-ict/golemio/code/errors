import { AbstractGolemioError } from "./AbstractGolemioError";
import { IGolemioErrorInfoType } from "./interfaces/GolemioErrorInterface";

/**
 * @deprecated For backwards compatibility.
 */
export class CustomError extends AbstractGolemioError {
    public isOperational?: boolean;

    constructor(
        message: string, isOperational?: boolean, className?: string, status?: number, info?: IGolemioErrorInfoType,
    ) {
        super(message, className, info, status);
        this.isOperational = isOperational;
    }
}
