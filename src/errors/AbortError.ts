import { FatalError } from "./FatalError";

export class AbortError extends FatalError {}
