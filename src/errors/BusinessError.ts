import { GeneralError } from "./GeneralError";

export class BusinessError extends GeneralError {}
