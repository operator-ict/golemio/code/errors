import { GeneralError } from "./GeneralError";

export class TransformationError extends GeneralError {}
