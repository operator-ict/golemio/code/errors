export interface IGolemioError {
    error_message: string;
    error_class_name?: string;
    error_status?: number;
    error_info?: string;
    error_stack?: string;
    error_module_name?: string;
    error_queue_name?: string;
}

/*
 * @deprecated For backwards compatibility.
 */
export interface ICustomErrorObject extends IGolemioError {}

export type IGolemioErrorInfoType = Error | IGolemioErrorInfoObject | IGolemioErrorInfoObject[] | string;

interface IGolemioErrorInfoObject {
    message: string;
    [key: string]: unknown;
}
