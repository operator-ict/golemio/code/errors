import { GeneralError } from "./GeneralError";

export class ValidationError extends GeneralError {}
