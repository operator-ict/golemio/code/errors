import { IGolemioErrorInfoType } from "./interfaces/GolemioErrorInterface";
import { IGolemioError } from "./interfaces/GolemioErrorInterface";

export abstract class AbstractGolemioError extends Error {
    public className?: string;
    public status?: number;
    public info?: IGolemioErrorInfoType;

    constructor(
        message: string,
        className?: string,
        info?: IGolemioErrorInfoType,
        status?: number,
        public moduleName?: string,
        public queueName?: string,
    ) {
        super(message);
        this.name = this.constructor.name;
        this.message = message;
        this.className = className;
        this.info = info;
        this.status = status;
        this.stack = (new Error()).stack;
    }

    public toString = (): string => {
        return `${this.className ? `${this.className}: ` : ``}`
            + `${this.status ? `[${this.status}] ` : ``}`
            + this.message
            + `${this.info ? ` (${this.infoToString(this.info)})` : ``}`
            + `${this.moduleName ? ` module: ${this.moduleName}` : ``}`
            + `${this.queueName ? ` queue: ${this.queueName}` : ``}`
            + `${process.env.NODE_ENV === "development" ? `\n ${this.stack}` : ``}`;
    }

    public toObject = (): IGolemioError => ({
        error_message: this.message,
        ...(this.className && { error_class_name: this.className }),
        ...(this.status && { error_status: this.status }),
        ...this.info && { error_info: this.infoToString(this.info) },
        ...(process.env.NODE_ENV === "development") && { error_stack: this.stack },
        ...(this.moduleName && { error_module_name: this.moduleName }),
        ...(this.queueName && { error_queue_name: this.queueName }),
    })

    private infoToString(info: IGolemioErrorInfoType): string {
        if (info instanceof AbstractGolemioError) {
            return info.toString();
        } else if (info instanceof Error) {
            return info.message;
        } else {
            return typeof info === "string" ? info : JSON.stringify(info);
        }
    }

}
