import { AbstractGolemioError } from "./AbstractGolemioError";

export class GeneralError extends AbstractGolemioError {}
