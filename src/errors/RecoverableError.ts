import { GeneralError } from "./GeneralError";

export class RecoverableError extends GeneralError {}
