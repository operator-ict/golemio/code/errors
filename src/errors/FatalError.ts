import { AbstractGolemioError } from "./AbstractGolemioError";
import { IGolemioErrorInfoType } from "./interfaces/GolemioErrorInterface";

export class FatalError extends AbstractGolemioError {
    constructor(
        message: string, className?: string, info?: IGolemioErrorInfoType, moduleName?: string, queueName?: string,
    ) {
        super(message, className, info, undefined, moduleName, queueName);
    }
}
