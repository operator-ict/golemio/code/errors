import { GeneralError } from "../errors/GeneralError";
import { IGolemioError } from "../errors/interfaces/GolemioErrorInterface";
import { ErrorHandler } from "./ErrorHandler";

export class HTTPErrorHandler extends ErrorHandler {

    public static handle<Logger extends Record<any, any>>(
        err: Error | GeneralError,
        logger: Logger,
        logLevel: keyof Logger = "error",
    ): IGolemioError {

        // Call super to handle unexpected errors
        const errObject: IGolemioError = super.handle(err, logger, logLevel);

        let toReturn: IGolemioError;

        switch (errObject.error_status) {
            // CLIENT ERRORS
            case 400: {
                toReturn = {
                    error_message: "Bad request",
                    error_status: 400,
                    ...(errObject.error_info && { error_info: errObject.error_info }),
                };
                break;
            }
            case 401: {
                toReturn = {
                    error_message: "Unauthorized. Failed to authenticate user.",
                    error_status: 401,
                };
                break;
            }
            case 403: {
                toReturn = {
                    error_message: "Forbidden. Access to request resource denied.",
                    error_status: 403,
                };
                break;
            }
            case 404: {
                toReturn = { error_message: "Not Found", error_status: 404 };
                break;
            }
            case 405: {
                toReturn = { error_message: "Method Not Allowed", error_status: 405 };
                break;
            }
            case 406: {
                toReturn = { error_message: "Not Acceptable", error_status: 406 };
                break;
            }
            case 407: {
                toReturn = { error_message: "Proxy Authentication Required", error_status: 407 };
                break;
            }
            case 408: {
                toReturn = { error_message: "Request Timeout", error_status: 408 };
                break;
            }
            case 409: {
                toReturn = { error_message: "Conflict", error_status: 409 };
                break;
            }
            case 410: {
                toReturn = { error_message: "Gone", error_status: 410 };
                break;
            }
            case 411: {
                toReturn = { error_message: "Length Required", error_status: 411 };
                break;
            }
            case 412: {
                toReturn = { error_message: "Precondition Failed", error_status: 412 };
                break;
            }
            case 413: {
                toReturn = {
                    error_message: "Payload Too Large. Try using ?limit parameter with lower number, if applicable.",
                    error_status: 413,
                };
                break;
            }
            case 414: {
                toReturn = { error_message: "URI Too Long", error_status: 414 };
                break;
            }
            case 415: {
                toReturn = { error_message: "Unsupported Media Type", error_status: 415 };
                break;
            }
            case 416: {
                toReturn = { error_message: "Requested Range Not Satisfiable", error_status: 416 };
                break;
            }
            case 417: {
                toReturn = { error_message: "Expectation Failed", error_status: 417 };
                break;
            }
            case 418: {  // I'm a teapot, don't try to brew coffee with me
                toReturn = { error_message: "I'm a teapot", error_status: 418 };
                break;
            }
            case 421: {
                toReturn = { error_message: "Misdirected Request", error_status: 421 };
                break;
            }
            case 422: {
                toReturn = {
                    error_message: "Unprocessable Entity",
                    error_status: 422,
                    ...(errObject.error_info && { error_info: errObject.error_info }),
                };
                break;
            }
            case 423: {
                toReturn = { error_message: "Locked", error_status: 423 };
                break;
            }
            case 424: {
                toReturn = { error_message: "Failed Dependency", error_status: 424 };
                break;
            }
            case 425: {
                toReturn = { error_message: "Too Early", error_status: 425 };
                break;
            }
            case 426: {
                toReturn = { error_message: "Upgrade Required", error_status: 426 };
                break;
            }
            case 428: {
                toReturn = { error_message: "Precondition Required", error_status: 428 };
                break;
            }
            case 429: {
                toReturn = { error_message: "Too Many Requests", error_status: 429 };
                break;
            }
            case 431: {
                toReturn = { error_message: "Request Header Fields Too Large", error_status: 431 };
                break;
            }
            case 444: {
                toReturn = { error_message: "Connection Closed Without Response", error_status: 444 };
                break;
            }
            case 451: {
                toReturn = { error_message: "Unavailable For Legal Reasons", error_status: 451 };
                break;
            }
            case 499: {
                toReturn = { error_message: "Client Closed Request", error_status: 499 };
                break;
            }
            // /CLIENT ERRORS

            // SERVER ERRORS
            case 500: {
                toReturn = { error_message: "Internal Server Error", error_status: 500 };
                break;
            }
            case 501: {
                toReturn = { error_message: "Not Implemented", error_status: 501 };
                break;
            }
            case 502: {
                toReturn = { error_message: "Not Implemented", error_status: 502 };
                break;
            }
            case 503: {
                toReturn = { error_message: "Service Unavailable", error_status: 503 };
                break;
            }
            case 504: {
                toReturn = { error_message: "Gateway Timeout", error_status: 504 };
                break;
            }
            case 505: {
                toReturn = { error_message: "HTTP Version Not Supported", error_status: 505 };
                break;
            }
            case 506: {
                toReturn = { error_message: "Variant Also Negotiates", error_status: 506 };
                break;
            }
            case 507: {
                toReturn = { error_message: "Insufficient Storage", error_status: 507 };
                break;
            }
            case 508: {
                toReturn = { error_message: "Loop Detected", error_status: 508 };
                break;
            }
            case 510: {
                toReturn = { error_message: "Not Extended", error_status: 510 };
                break;
            }
            case 511: {
                toReturn = { error_message: "Network Authentication Required", error_status: 511 };
                break;
            }
            case 599: {
                toReturn = { error_message: "Network Connect Timeout Error", error_status: 599 };
                break;
            }
            default: {
                toReturn = { error_message: "Unknown Server Error", error_status: 520 };
            }
        }

        // If we're in development, add stack trace to the error object
        if (process.env.NODE_ENV === "development") {
            toReturn.error_stack = err.stack;
        }
        return toReturn;
    }
}
