import { AbstractGolemioError } from "../errors/AbstractGolemioError";
import { CustomError } from "../errors/CustomError";
import { FatalError } from "../errors/FatalError";
import { GeneralError } from "../errors/GeneralError";
import { IGolemioError } from "../errors/interfaces/GolemioErrorInterface";

export class ErrorHandler {

    public static handle<Logger extends Record<any, any>>(
        err: Error | GeneralError,
        logger: Logger,
        logLevel: keyof Logger = "error",
    ): IGolemioError {
        if (
            (err instanceof AbstractGolemioError && !(err instanceof FatalError || err instanceof CustomError)) ||
            (err instanceof CustomError && err.isOperational)
        ) {
            logger[logLevel](err);
            return err.toObject();
        } else {
            // Unexpected and Fatal errors
            logger.error(err.toString());
            return process.exit((err instanceof AbstractGolemioError && err.status) ? err.status : 1);
        }
    }
}
