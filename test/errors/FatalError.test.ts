import * as chai from "chai";
import { expect } from "chai";
import * as chaiAsPromised from "chai-as-promised";

import { FatalError } from "../../src";

chai.use(chaiAsPromised);

describe("FatalError", () => {
    let error1: FatalError;
    let error2: FatalError;
    let error3: FatalError;

    let tmpNodeEnv: string | undefined;

    before(() => {
        tmpNodeEnv = process.env.NODE_ENV;
        process.env.NODE_ENV = "test";
    });

    after(() => {
        process.env.NODE_ENV = tmpNodeEnv;
    });

    beforeEach(() => {
        error1 = new FatalError("Error message");
        error2 = new FatalError("Error message", "TestError");
        error3 = new FatalError("Error message", "TestError", new Error("Additional info"));
    });

    describe("'toString' method", () => {
        it("should return error description as a string", async () => {
            expect(error1.toString()).to.be.equal("Error message");
            expect(error2.toString()).to.be.equal("TestError: Error message");
            expect(error3.toString()).to.be.equal("TestError: Error message (Additional info)");
        });
    });

    describe("toObject method", () => {
        it("should return error description as object", async () => {
            expect(error1.toObject()).to.eql({error_message: "Error message"});
            expect(error2.toObject()).to.eql({error_message: "Error message", error_class_name: "TestError"});
            expect(error3.toObject()).to.eql({
                error_message: "Error message",
                error_class_name: "TestError",
                error_info: "Additional info",
            });
        });

        it("should contain stack trace if node_env is development", () => {
            process.env.NODE_ENV = "development";
            expect(error1.toObject()).to.have.property("error_stack");
        });
    });
});
