import * as chai from "chai";
import { expect } from "chai";
import * as chaiAsPromised from "chai-as-promised";

import { GeneralError } from "../../src";

chai.use(chaiAsPromised);

describe("GeneralError", () => {
    let error1: GeneralError;
    let error2: GeneralError;
    let error3: GeneralError;
    let error4: GeneralError;
    let error5: GeneralError;

    let tmpNodeEnv: string | undefined;

    before(() => {
        tmpNodeEnv = process.env.NODE_ENV;
        process.env.NODE_ENV = "test";
    });

    after(() => {
        process.env.NODE_ENV = tmpNodeEnv;
    });

    beforeEach(() => {
        error1 = new GeneralError("Error message");
        error2 = new GeneralError("Error message", "GeneralError");
        error3 = new GeneralError("Error message", "GeneralError", new Error("Additional info"));
        error4 = new GeneralError("Error message", "GeneralError", new Error("Additional info"), 400);
        error5 = new GeneralError("Error message", "GeneralError", new Error("Additional info"), 400, "modul", "queue");
    });

    describe("'toString' method", () => {
        it("should return error description as a string", async () => {
            expect(error1.toString()).to.be.equal("Error message");
            expect(error2.toString()).to.be.equal("GeneralError: Error message");
            expect(error3.toString()).to.be.equal("GeneralError: Error message (Additional info)");
            expect(error4.toString()).to.be.equal("GeneralError: [400] Error message (Additional info)");
            expect(error5.toString()).to.be.equal("GeneralError: [400] Error message (Additional info) module: modul queue: queue");
        });
    });

    describe("toObject method", () => {
        it("should return error description as object", async () => {
            expect(error1.toObject()).to.eql({error_message: "Error message"});
            expect(error2.toObject()).to.eql({error_message: "Error message", error_class_name: "GeneralError"});
            expect(error3.toObject()).to.eql(
                {error_message: "Error message", error_class_name: "GeneralError", error_info: "Additional info"},
            );
            expect(error4.toObject()).to.eql({
                error_message: "Error message",
                error_class_name: "GeneralError",
                error_status: 400,
                error_info: "Additional info",
            });
            expect(error5.toObject()).to.eql({
                error_message: "Error message",
                error_class_name: "GeneralError",
                error_status: 400,
                error_info: "Additional info",
                error_module_name: "modul",
                error_queue_name: "queue",
            });
        });

        it("should contain stack trace if node_env is development", () => {
            process.env.NODE_ENV = "development";
            expect(error1.toObject()).to.have.property("error_stack");
            process.env.NODE_ENV = tmpNodeEnv;
        });
    });

    describe("infoToString method", () => {
        it("should return error info description string", async () => {
            const error = new GeneralError("Dummy");
            expect((error as any).infoToString(new Error("Info error"))).to.eql("Info error");
            expect((error as any).infoToString(new GeneralError("Info error"))).to.eql("Info error");
            expect((error as any).infoToString({ message: "Info error" })).to.eql("{\"message\":\"Info error\"}");
            expect((error as any).infoToString([{ message: "Info error" }])).to.eql("[{\"message\":\"Info error\"}]");
            expect((error as any).infoToString("Info error")).to.eql("Info error");
        });
    });
});
