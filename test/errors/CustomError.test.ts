import * as chai from "chai";
import { expect } from "chai";
import * as chaiAsPromised from "chai-as-promised";

import { CustomError } from "../../src";

chai.use(chaiAsPromised);

describe("CustomError", () => {

    let error1: CustomError;
    let error2: CustomError;
    let error3: CustomError;
    let error4: CustomError;
    let error5: CustomError;
    let tmpNodeEnv: string | undefined;

    before(() => {
        tmpNodeEnv = process.env.NODE_ENV;
        process.env.NODE_ENV = "test";
    });

    after(() => {
        process.env.NODE_ENV = tmpNodeEnv;
    });

    beforeEach(() => {
        error1 = new CustomError("Test error");
        error2 = new CustomError("Test error", true);
        error3 = new CustomError("Test error", true, "TestError");
        error4 = new CustomError("Test error", true, "TestError", 4);
        error5 = new CustomError("Test error", true, "TestError", 5, "Additional info");
    });

    describe("'toString' method", () => {
        it("should return error description as a string", async () => {
            expect(error1.toString()).to.be.equal("Test error");
            expect(error2.toString()).to.be.equal("Test error");
            expect(error3.toString()).to.be.equal("TestError: Test error");
            expect(error4.toString()).to.be.equal("TestError: [4] Test error");
            expect(error5.toString()).to.be.equal("TestError: [5] Test error (Additional info)");
        });
    });

    describe("toObject method", () => {
        it("should return error description as object", async () => {
            expect(error1.toObject()).to.have.property("error_message");
            expect(error2.toObject()).to.have.property("error_message");
            expect(error3.toObject()).to.have.property("error_message");
            expect(error3.toObject()).to.have.property("error_class_name");
            expect(error4.toObject()).to.have.property("error_message");
            expect(error4.toObject()).to.have.property("error_class_name");
            expect(error4.toObject()).to.have.property("error_status");
            expect(error5.toObject()).to.have.property("error_message");
            expect(error5.toObject()).to.have.property("error_class_name");
            expect(error5.toObject()).to.have.property("error_status");
            expect(error5.toObject()).to.have.property("error_info");
        });

        it("should contain stack trace if node_env is development", () => {
            process.env.NODE_ENV = "development";
            expect(error1.toObject()).to.have.property("error_stack");
        });
    });
});
