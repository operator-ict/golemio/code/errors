import * as chai from "chai";
import { expect } from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as sinon from "sinon";

import { CustomError, FatalError, GeneralError, HTTPErrorHandler } from "../../src";

chai.use(chaiAsPromised);

describe("HTTPErrorHandler", () => {
    let sandbox: sinon.SinonSandbox;
    let exitStub: sinon.SinonSpy;
    let tmpNodeEnv: string | undefined;

    const loggerMock = { error: () => null };

    before(() => {
        tmpNodeEnv = process.env.NODE_ENV;
        process.env.NODE_ENV = "test";
    });

    after(() => {
        process.env.NODE_ENV = tmpNodeEnv;
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        exitStub = sandbox.stub(process, "exit").callsFake(() => ({}) as never);
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("Unexpected error", () => {
        it("should handle unexpected error and exit the app", async () => {
            HTTPErrorHandler.handle(new Error("Error message"), loggerMock);
            sinon.assert.called(exitStub);
        });

        it("should handle fatal error and exit the app", async () => {
            HTTPErrorHandler.handle(new FatalError("Error message"), loggerMock);
            sinon.assert.called(exitStub);
        });

        it("should handle deprecated non-operational error and exit with specific error code", async () => {
            HTTPErrorHandler.handle(new CustomError("Error message", false, "TestError", 1001), loggerMock);
            sinon.assert.called(exitStub);
            sinon.assert.calledWith(exitStub, 1001);
        });

    });

    describe("Expected error", () => {
        it("should handle 400 error", async () => {
            const res = HTTPErrorHandler.handle(
                new GeneralError("Error message", "TestError", new Error("Additional info"), 400), loggerMock,
            );
            sinon.assert.notCalled(exitStub);
            expect(res.error_status).to.be.equal(400);
        });

        it("should handle 404 error", async () => {
            const res = HTTPErrorHandler.handle(
                new GeneralError("Error message", "TestError", new Error("Additional info"), 404), loggerMock,
            );
            sinon.assert.notCalled(exitStub);
            expect(res.error_status).to.be.equal(404);
        });

        it("should handle 409 error", async () => {
            const res = HTTPErrorHandler.handle(
                new GeneralError("Error message", "TestError", new Error("Additional info"), 409), loggerMock,
            );
            sinon.assert.notCalled(exitStub);
            expect(res.error_status).to.be.equal(409);
        });

        it("should handle 422 error", async () => {
            const err = HTTPErrorHandler.handle(
                new GeneralError("Error message", "TestError", new GeneralError("Additional info"), 422), loggerMock,
            );
            sinon.assert.notCalled(exitStub);
            expect(err).to.deep.equal({
                error_info: "Additional info",
                error_message: "Unprocessable Entity",
                error_status: 422,
            });
        });

        it("should handle 422 error (with GeneralError)", async () => {
            const err = HTTPErrorHandler.handle(
                new GeneralError(
                    "Error message",
                    "TestError",
                    new GeneralError("Info error message", "TestError", new Error("Additional info")),
                    422,
                ), loggerMock);
            sinon.assert.notCalled(exitStub);
            expect(err).to.deep.equal({
                error_info: "TestError: Info error message (Additional info)",
                error_message: "Unprocessable Entity",
                error_status: 422,
            });
        });

        it("should handle 422 error (with object)", async () => {
            const err = HTTPErrorHandler.handle(new GeneralError("Error message", "TestError",
                [{message: "Invalid value", param: "test", location: "body"}], 422), loggerMock,
            );
            sinon.assert.notCalled(exitStub);
            expect(err).to.deep.equal({
                error_info: "[{\"message\":\"Invalid value\",\"param\":\"test\",\"location\":\"body\"}]",
                error_message: "Unprocessable Entity",
                error_status: 422,
            });
        });

        it("should handle 500 error", async () => {
            const res = HTTPErrorHandler.handle(
                new GeneralError("Server error test", "TestError", undefined, 500), loggerMock,
            );
            sinon.assert.notCalled(exitStub);
            expect(res.error_status).to.be.equal(500);
        });

        it("should handle unspecified (unknown) error", async () => {
            const res = HTTPErrorHandler.handle(new GeneralError("No idea what happened"), loggerMock);
            sinon.assert.notCalled(exitStub);
            expect(res.error_status).to.be.equal(520);
        });

        it("should contain stack trace if node_env is development", () => {
            process.env.NODE_ENV = "development";
            const res = HTTPErrorHandler.handle(
                new GeneralError("Error message", "TestError", new Error("Additional info"), 400), loggerMock,
            );
            expect(res).to.have.property("error_stack");
        });
    });
});
