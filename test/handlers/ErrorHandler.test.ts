import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as sinon from "sinon";

import { CustomError, ErrorHandler, FatalError, GeneralError } from "../../src";

chai.use(chaiAsPromised);

describe("ErrorHandler", () => {
    let sandbox: sinon.SinonSandbox;
    let exitStub: sinon.SinonSpy;
    let tmpNodeEnv: string | undefined;

    const loggerMock = { error: () => null };

    before(() => {
        tmpNodeEnv = process.env.NODE_ENV;
        process.env.NODE_ENV = "test";
    });

    after(() => {
        process.env.NODE_ENV = tmpNodeEnv;
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        exitStub = sandbox.stub(process, "exit");
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("Unexpected error", () => {
        it("should handle unexpected error (Error)", async () => {
            ErrorHandler.handle(new Error("Error message"), loggerMock);
            sinon.assert.called(exitStub);
            sinon.assert.calledWith(exitStub, 1);
        });

        it("should handle unexpected error (FatalError)", async () => {
            ErrorHandler.handle(new FatalError("Error message"), loggerMock);
            sinon.assert.called(exitStub);
            sinon.assert.calledWith(exitStub, 1);
        });

        it("should handle deprecated non-operational error and exit with specific error code", async () => {
            ErrorHandler.handle(new CustomError("Error message", false, "TestError", 1001), loggerMock);
            sinon.assert.called(exitStub);
            sinon.assert.calledWith(exitStub, 1001);
        });
    });

    describe("Expected error", () => {
        it("should handle application error", async () => {
            ErrorHandler.handle(
                new GeneralError("Error message", "TestError", new Error("Additional info")), loggerMock,
            );
            sinon.assert.notCalled(exitStub);
        });

        it("should handle deprecated operational error", async () => {
            ErrorHandler.handle(new CustomError("Error message", true, "TestError"), loggerMock);
            sinon.assert.notCalled(exitStub);
        });

    });
});
