# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.0.5] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [2.0.4] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [2.0.3] - 2024-02-12

### Added

- Add module and queue names for logging ([core#92](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/92))

## [2.0.2] - 2023-08-21

### Added

- Abort error ([core#64](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/64))

## [2.0.1] - 2023-06-14

### Changed

- Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [2.0.0] - 2023-05-31

### Changed

- Refactor errors module ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.1.1] - 2023-02-26

### Changed

- Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

- Migrate to npm

## [1.0.2] - 2022-11-29

### Changed

- remove default winston logger, pass logger as mandatory argument

